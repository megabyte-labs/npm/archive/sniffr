import { validate } from 'class-validator';
import { Logger } from './lib/log';
import { flatten, createDir } from './lib/util';
import { SniffrOptions } from './models/sniffr-options.model';
import { findOccurrences } from './lib/scan-files';
import { writeFile } from './lib/util';

export class Sniffr {
  /**
   * This is the master method that will import the JSON, scan the directory, and generate results.
   *
   * @param data Options in the form of [[SniffrOptions]]
   * @returns A void Promise
   */
  public static async run(data: SniffrOptions): Promise<void> {
    const config = new SniffrOptions(data);
    const errors = await validate(config);
    if (errors.length) {
      Logger.error(errors);
    } else {
      // SniffrOptions is valid
      const flattened = flatten(config.input);
      Logger.info('Filtering unique keys');
      const keys = [];
      const arr = Object.keys(flattened).filter((v, i, a) => {
        const key = v.indexOf('.') !== -1 ? v.substr(v.lastIndexOf('.') + 1) : v;
        return keys.indexOf(key) === -1 && isNaN(parseInt(key)) ? keys.push(key) : false;
      });
      Logger.log('Filtered keys:', arr);
      const raw = arr.map(x => {
        const key = x.indexOf('.') !== -1 ? x.substr(x.lastIndexOf('.') + 1) : x;
        Logger.log('Scanning for ' + key);
        const data = findOccurrences(config.dir, key);
        const count = data.length;
        return {
          count,
          item: x,
          rawData: data,
          searchKey: key
        }
      });
      Logger.info('Saving results to ./sniffr-results');
      await createDir('./sniffr-results');
      await writeFile('./sniffr-results/raw.json', JSON.stringify(raw));
      const notFound = raw.filter(x => !x.count).map(x => x.item);
      await writeFile('./sniffr-results/not-found.json', JSON.stringify(notFound));
      const found = raw.filter(x => x.count).map(x => x.item);
      await writeFile('./sniffr-results/found.json', JSON.stringify(found));
    }
  }
}
