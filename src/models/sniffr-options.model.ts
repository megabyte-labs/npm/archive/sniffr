export class SniffrOptions {
  dir: string;
  input: string;

  constructor(data: SniffrOptions) {
    this.dir = data.dir ? data.dir : './';
    this.input = data.input ? data.input : './input.json';
  }
}
