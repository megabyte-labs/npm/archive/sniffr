export const CLIOptions = {
    prepend: 'Usage: sniffr [options]',
    append: 'Version 1.x.x',
    options: [{
        option: 'help',
        alias: 'h',
        type: 'Boolean',
        description: 'Displays help'
    }, {
        option: 'json',
        alias: 'j',
        type: 'String',
        description: 'Relative path to the JSON that you would like to use for sniffing. Default is ./input.json',
        example: 'sniffr --json ./input.json'
    }, {
      option: 'dir',
      alias: 'd',
      type: 'String',
      description: 'Directory to scan. Default is .',
      example: 'sniffr --json ./myfile.json --dir ./folder/to/scan'
    }]
}
