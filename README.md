# @tainted/objectkeysniffer

Flattens an object, scans a folder, and returns line locations of the object keys. Useful for finding used/unused pieces of data from server responses.
