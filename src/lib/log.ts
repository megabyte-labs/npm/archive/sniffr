import { default as Pino } from 'pino';

export const pinoLog = Pino({
  prettyPrint: {
    ignore: 'hostname,pid',
    translateTime: 'SYS:HH:MM:ss.l o'
  }
});

export class Logger {
  public static debug(...args): void {
    pinoLog.debug(...args);
  }

  public static error(...args): void {
    pinoLog.error(...args);
  }

  public static info(...args): void {
    pinoLog.info(...args);
  }

  public static log(...args): void {
    pinoLog.info(...args);
  }

  public static warn(...args): void {
    pinoLog.warn(...args);
  }
}
