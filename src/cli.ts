import * as fs from 'fs';
import * as path from 'path';
import { Sniffr } from './app';
import { CLIOptions } from './constants/cli-options.constant';
import { Logger } from './lib/log';

const optionator = require('optionator')(CLIOptions);

/** Delegate CLI commands to appropriate logic */
export async function cli(argz: any) {
  const options = optionator.parseArgv(argz);
  // TODO: Move this logic to the class-validator model
  const jsonPath = options.json ? options.json : './input.json';
  if (options.help) {
    return console.log(optionator.generateHelp());
  } else {
    const pathh = path.resolve(process.cwd(), jsonPath);
    if (fs.existsSync(pathh)) {
      if (fs.lstatSync(pathh).isDirectory()) {
        Logger.error(
          'The path to the input JSON file you specified appears to be a directory. A JSON file was expected.'
        );
      } else {
        try {
          const json = await import(pathh);
          Sniffr.run({
            dir: options.dir,
            input: json
          });
        } catch (e) {
          Logger.error(
            'Failed to import the JSON file. Something is probably wrong with the JSON - try validating it.'
          );
        }
      }
    } else {
      Logger.error('The path to the JSON file you specified (' + pathh + ') does not exist');
    }
  }
}
