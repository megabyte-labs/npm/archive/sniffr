import * as fs from 'fs';
import jsonabc from 'jsonabc';
import { Logger } from './log';
import { globber, writeFile } from './util';

export async function sortJSON(patterns: readonly string[]) {
  const matches: any = await Promise.all(patterns.map(async x => globber(x)));
  const flattened = matches.flat();
  await Promise.all(
    flattened.map(async x => {
      const input = fs.readFileSync(x).toString();
      await writeFile(x, jsonabc.sort(input, false).toString());
    })
  );
}
