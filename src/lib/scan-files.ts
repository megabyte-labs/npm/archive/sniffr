import { fastFindInFiles } from 'fast-find-in-files';

/**
 * Scans directory for a string and returns file information about the
 * occurrences. See [fast-find-in-files](https://www.npmjs.com/package/fast-find-in-files).
 *
 * @param dir The directory to scan
 * @param str The string to search for
 * @returns Information about the occurrences
 */
export function findOccurrences(dir: string, str: string): any[] {
  return fastFindInFiles(dir, str);
}
