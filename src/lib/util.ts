import * as fs from 'fs';
import glob from 'glob';
import flat from 'flat';
import { isEqual, isObject, transform } from 'lodash';
import * as path from 'path';
import * as prettier from 'prettier';

/**
 * Recursively creates a directory
 *
 * @param path The path of the directory tree you want to create
 * @returns A promise that resolves when the operation is complete
 */
export function createDir(path: string): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.mkdir(path, { recursive: true }, error => {
      if (error) {
        reject('Failed to create directory ' + path);
      } else {
        resolve();
      }
    });
  });
}

/**
 * Compares two objects
 *
 * @param base The base object
 * @param obj The object you want to find the missing key/values of
 * @returns An object with the key/values that the obj is missing to be of base type
 */
export function diffObjects(base: {}, obj: {}): {} {
  return transform(obj, (result, value, key) => {
    if (!isEqual(value, base[key])) {
      if (!base[key]) {
        /* tslint:disable */
        result[key] = isObject(value) && isObject(base[key]) ? diffObjects(value, base[key]) : value;
        /* tslint:enable */
      }
    }
  });
}

/**
 * Return an array of files in a directory
 *
 * @param path The path to scan for files
 * @returns A promise that resolves an array of files
 */
export function dirFiles(pathh: string): Promise<readonly string[]> {
  return new Promise((resolve, reject) => {
    fs.readdir(pathh, (error, files) => {
      error ? reject(error) : resolve(files.map(x => path.resolve(pathh + '/' + x)));
    });
  });
}

/**
 * Flattens an object. For more information, see [flat](https://www.npmjs.com/package/flat).
 *
 * Example:
 * ```
 * var flatten = require('flat')
 * flatten({
 *   key1: {
 *       keyA: 'valueI'
 *   },
 *   key2: {
 *       keyB: 'valueII'
 *   },
 *   key3: { a: { b: { c: 2 } } }
 * })
 * // {
 * //   'key1.keyA': 'valueI',
 * //   'key2.keyB': 'valueII',
 * //   'key3.a.b.c': 2
 * // }
 * ```
 *
 * @param obj An object you wish to flatten
 * @returns The flattened object
 */
export function flatten(obj: any): {} {
  return flat(obj);
}

/**
 * Uses [Prettier](https://www.npmjs.com/package/prettier) to properly
 * format strings before saving them to files
 *
 * @param str The string to pass through [prettier](https://www.npmjs.com/package/prettier)
 * @returns A properly formatted string
 */
export function format(str: string, options: any = {}) {
  return prettier.format(str, options);
}

/**
 * Uses a glob pattern to find matching files
 *
 * @param pattern The pattern to match
 * @returns An array of files that match
 */
export function globber(pattern: string): Promise<readonly string[]> {
  return new Promise((resolve, reject) => {
    glob(pattern, null, (error, files) => {
      error ? reject(error) : resolve(files);
    });
  });
}

/**
 * Determines whether the file is valid (whether it exists or not for now)
 *
 * @param path The path to the file
 * @returns True if the file is valid
 */
export function validFile(path: string): boolean {
  return fs.existsSync(path);
}

/**
 * Saves a file
 *
 * @param path The absolute path to the file
 * @param data The data being written to the file
 * @returns A promise that resolves if the file was written without error
 */
export function writeFile(path: string, data: any): Promise<void> {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, data, error => {
      if (error) {
        reject('Failed to save ' + path);
      } else {
        resolve();
      }
    });
  });
}
